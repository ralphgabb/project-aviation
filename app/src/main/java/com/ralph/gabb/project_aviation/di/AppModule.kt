package com.ralph.gabb.project_aviation.di

/**
 * Created by Ralph Gabrielle Orden on 9/20/2019.
 */

val appModule = listOf (

    viewModelModule, repositoryModule, networkModule
)