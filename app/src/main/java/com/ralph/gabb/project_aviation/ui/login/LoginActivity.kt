package com.ralph.gabb.project_aviation.ui

import com.ralph.gabb.project_aviation.R
import com.ralph.gabb.project_aviation.base.BaseActivity
import com.ralph.gabb.project_aviation.ex.content
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/*
 * Created by Ralph Gabrielle Orden on 2019-10-19.
 */
class LoginActivity : BaseActivity() {

    private val viewModel: LoginViewModel by viewModel()

    override val layoutId: Int?
        get() = R.layout.activity_login

    override fun viewCreated() {

        bLogin.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val userName = etUsername.content()
        val password = etPassword.content()

        viewModel.login()
    }
}